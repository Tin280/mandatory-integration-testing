const request = require('supertest');
const app = require('../src/server'); 

describe('API Routes', () => {
  test('should convert hex to rgb', async () => {
    const hex = 'FF5733'; // Replace with your hex color
    const expectedRgb = { r: 255, g: 87, b: 51 }

    const response = await request(app)
      .get(`/hex-to-rgb/${hex}`)
      .expect(200);

    // Replace the .to.deep.equal with Jest's expect
    expect(response.body.rgb).toEqual(expectedRgb)

  });
  test('should handle hex input with a "#" prefix', async () => {
    const hexColor = '#FF5733'
    const expectedRgb = { r: 255, g: 87, b: 51 }

    const response = await request(app)
      .post(`/hex-to-rgb/`)
      .send({hex: hexColor})
      .expect(200);
    
    expect(response.body.rgb).toEqual(expectedRgb)
    // console.log(response.body.rgb)
  })
});
