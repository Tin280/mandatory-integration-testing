
const { hexToRgb } = require('../src/converter'); // Import your hexToRgb function


describe('hexToRgb', () => {
  test('should convert hex to rgb', () => {
    const hexColor = 'FF5733'; // Replace with your hex color
    const expectedRgb = { r: 255, g: 87, b: 51 };
    const result = hexToRgb(hexColor);

    expect(result).toEqual(expectedRgb);
  });

  test('should handle lowercase hex input', () => {
    const hexColor = 'ff5733'; // Lowercase hex
    const expectedRgb = { r: 255, g: 87, b: 51 };
    const result = hexToRgb(hexColor);

    expect(result).toEqual(expectedRgb);
  });

  test('should handle hex input with a "#" prefix', () => {
    const hexColor = '#FF5733'; // Hex with "#" prefix
    const expectedRgb = { r: 255, g: 87, b: 51 };
    const result = hexToRgb(hexColor);

    expect(result).toEqual(expectedRgb);
  });

  test('should handle short hex input', () => {
    const hexColor = 'F53'; // Short hex (expanded to FF5533)
    const expectedRgb = { r: 255, g: 85, b: 51 };
    const result = hexToRgb(hexColor);

    expect(result).toEqual(expectedRgb);
  });

  test('should return null for invalid hex input', () => {
    const hexColor = 'invalid'; // Invalid hex
    const result = hexToRgb(hexColor);

    expect(result).toEqual.null;
  });
});
