const express = require('express');
const app = express();
const port = 3000;

app.use(express.json());

// Import the hexToRgb function from the converter module
const { hexToRgb } = require('./converter');

// API route for hex to RGB conversion
app.get('/hex-to-rgb/:hex', (req, res) => {
  const hex = req.params.hex;
  const rgb = hexToRgb(hex);
  console.log({ hex, rgb })
  
  res.json({ hex, rgb });
});

app.post('/hex-to-rgb', (req, res) => {
    const hex = req.body.hex;
    const rgb = hexToRgb(hex);
    console.log({ hex, rgb })
    res.json({ hex, rgb });
  });

if (process.env.NODE_ENV === 'test') {
    module.exports = app;
} else {
    app.listen(port, () => {
        console.log(`Server: localhost:${port}`)
    })
}
