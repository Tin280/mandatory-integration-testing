function hexToRgb(hex) {
  // Remove the "#" prefix if present
  hex = hex.replace(/^#/, '');

  // Validate the input hex color code
  if (!/^(?:[0-9a-fA-F]{3}){1,2}$/.test(hex)) {
    return null; // Invalid input
  }

  // Expand short hex format (e.g., "F53" to "FF5533")
  if (hex.length === 3) {
    hex = hex.replace(/(.)/g, '$1$1');
  }

  // Parse the hex values and convert to decimal
  const bigint = parseInt(hex, 16);
  const r = (bigint >> 16) & 255;
  const g = (bigint >> 8) & 255;
  const b = bigint & 255;

  return { r, g, b };
}

module.exports = {
  hexToRgb,
};
